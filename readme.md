# Html图文编辑器-emEditor  
emEditor是一款Html页面内容编辑器，基于原生JavaScript的
原型模式设计，可在PC浏览器和微信小程序中实现图文资料的所见即所
得编辑。通过内置的section元素编辑器可以完成对图文素材标题颜色
、位置调整，通过img元素编辑器可以完成对图片的边框、尺寸、
阴影、圆角、位置、自适应等美观调整。
![image](https://bnusport.cn/emeditor/images/pic1.jpg)
![image](https://bnusport.cn/emeditor/images/pic2.jpg)

***
在线预览地址：`https://www.bnusport.cn/emeditor/editor.html`  
手机小程序预览请扫码：  
 ![image](https://bnusport.cn/emeditor/images/emeditor.png)

## 1 获取方式  
1. npm  
2. 直接下载压缩包：https://gitee.com/emprogram/em-editor/repository/archive/master.zip
## 2 使用说明  
+ 2.1 创建实例  
    `let editor = new emEditor('id',{});`  
    id为html文档中的容器元素id；  
    {}为个性化参数，可配置菜单、ajax地址等信息。
+ 2.2. 参数说明  
   + 2.2.1配置菜单  
   编辑器默认提供撤销、重做、保存、字体大小、粗体、斜体、
   下划线、居左、居中、居右、背景颜色、字体颜色、缩进、
   减少缩进、清除格式、有序列表、无序列表、链接、取消链接、
   上标、下标、文件、直接上传图片、图片裁剪上传、音频、
   视频、代码、删除这29个基础操作菜单及方法。菜单json数据在
   js/emEditorData.js中  
   菜单图标使用SVG元素配置，可动态插入。  
   增加菜单示例：  
   `menu:{newIcon:[{index:12,name:'res',title:'资源库',event:'click',click: function (e) {}}],newSVG:resIcon}`  
     + newIcon: 数组，配置新增菜单的基础项数据；  
     菜单基础项参数说明：  
     index: 菜单图标位置索引，不写默认增加到最后；  
     name: 对应SVG图标中的symbol元素id，注意不用写icon-；  
     title: 悬浮提示文字；
     event: 菜单响应事件，支持html元素基础鼠标事件；  
     click: 鼠标响应事件方法，标签对应event中配置的菜单响应事件；  
     + newSVG：字符串，新增SVG图标脚本；  
   + 2.2.2配置显示内容  
   实例化对象过程中可以直接增加需要编辑的html页面内容。示例：  
   `content:'<p>新建后直接可以看到我</p>'`
   + 2.2.3微信支持  
   如需在微信小程序的web-view组件中使用本编辑器，则需要在head中引入：  
   `<script src="https://res.wx.qq.com/open/js/jweixin-1.6.0.js"></script>`  
   参数中配置wxConfig:{url:'https://example.com',data:{}};
   url为服务器端api地址，可返回微信配置jsjdk所需参数，具体脚本可参考cSharp目录中的C#源码
   + 2.2.4公共方法  
     + SetContent(html)：设置编辑器中的编辑html内容；
     + SetMenuEnabled(name,enable)：按菜单名称设置菜单是否可用；
     + SetMenuEvent(name,event,fun)：按菜单名称重置菜单响应事件；
     + Ajax({url:'',type: 'GET',dataType: 'json',success:function(){}}): ajax方法；
     + InsertImage(imgs)：在当前文档位置插入图片，imgs为数组，每个元素至少需标注src，支持img元素所有属性的注入；
     + InsertHtml(html)：在当前文档位置插入有效地html脚本；
     + ShowDialog(title,style,obj,buttons)：弹出模式化窗口；
     + GetContent()：获取当前编辑器中内容的html语法；
     + Save(parm,success): 将文档中的base64文件内容上传服务器
     + 未完待续...  
# 开源协议  
   本编辑器遵守BSD开源协议，可以商用及个人学习使用，并修改调整任意代码。  
# 关于作者  
   蒲子明，Javascript超级粉丝，自由软件开发者，20余年大型国企信息化开发经验，现致力于编程教育，愿意帮助每一个积极上进的程序爱好者！  
   QQ：407342820  
   email：puziming@163.com  
# 版本更新日志   
   + V1.0.0   2020年10月28日  
     1 实现http基础功能  
     2 img元素编辑功能  
     3 section元素编辑功能
   + V.1.0.1  2020年11月25日  
     1 新增处理微信端上传图片，增加ajaxConfig参数，可进行打开、保存、图片上传等功能参数设置  
     2 新增token认证功能，需在调用src最后用#分隔token码，系统每次ajax将会自动在header中携带token
